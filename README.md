# README #
A modified of the DirectX Tool Kit Simple Sample to demonstration simple SpriteSheet animation.

### How do I get set up? ###
* Open 'UFOAnimation_Desktop_2013.sln' using Visual Studio 2013.

### Licenses ###
* DirectX Tool Kit is distributed under MS-PL
* DXTK Simple Sample is distributed under MP-LPL

### Contributors ###
* Paul Boocock
* DirectX Tool Kit (http://directxtk.codeplex.com/)
* Based off DXTK Simple Sample (https://code.msdn.microsoft.com/DirectXTK-Simple-Win32-23db418a)